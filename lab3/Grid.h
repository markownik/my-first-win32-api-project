#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include "Square.h"
using namespace std;
string buff;


class GRID {
private:
	int square_size = 30,
		w = 3,
		h = 3,
		spacing = 2;
	vector <SQUARE> squares;
	COORD position;

	void generate(void)
	{
		for (int i = 0; i < (w*h); i++)
		{
			int sx, sy, row, col;
			row = i / w;// -i % w;
			col = i % w;
			sx = col * (square_size + spacing) + spacing + position.X;
			sy = row * (square_size + spacing) + spacing + position.Y;
			squares.push_back(SQUARE(sx, sy, square_size, 0));
		}
	}
public:
	GRID(void)
	{
		position.X = 0;
		position.Y = 0;
		buff = "New Grid - size: " + to_string(w) + "x" + to_string(h) + "\n";
		OutputDebugString(buff.c_str());
		generate();
	}

	GRID(int _w, int _h)
	{
		position.X = 0;
		position.Y = 0;
		w = _w;
		h = _h;
		buff = "New Grid - size: " + to_string(w) + "x" + to_string(h) + "\n";
		OutputDebugString(buff.c_str());
		generate();
	}

	void setSquareSize(int size)
	{
		square_size = size;
		for (unsigned i = 0; i < squares.size(); ++i)
		{
			squares[i].setSize(size);
		}
	}

	void setPosition(int _x, int _y)
	{
		position.X = _x;
		position.Y = _y;
		for (unsigned i = 0; i < squares.size(); ++i)
		{
			int sx, sy, row, col;
			row = i / w;// -i % w;
			col = i % w;
			sx = col * (square_size + spacing) + spacing + position.X;
			sy = row * (square_size + spacing) + spacing + position.Y;
			squares[i].setPosition(sx, sy);
		}
	}

	int getSize(void)
	{
		return squares.size();// + 1;
	}

	void adjustToPossibleSpace(int _x, int _y, int _w, int _h)
	{
		float scalex = (float)(_w - 2*_x) / (float)(w * (square_size + spacing) + spacing), scaley = (float)(_h - 2*_y) / (float)(h  * (square_size + spacing) + spacing), scale = 1.0;
		if (scalex <= scaley)
			scale = scalex;
		else
			scale = scaley;

		setSquareSize(square_size * scale);
		setPosition(_x, _y);

	}

	int check(int _x, int _y)
	{
		bool found = false;
		unsigned i = 0;
		while (!found && (i < squares.size()))
		{
			RECT check = squares[i].getSquare();
			if (check.left <= _x && check.right >= _x && check.top <= _y && check.bottom >= _y)
				found = true;
			else
				i++;
		}
		if (found)
			return (int)i+1;
		else
			return 0;
	}

	void reset(void)
	{
		squares.clear();
		generate();
	}

	void tag(int num, int _t)
	{
		if (num > 0 && num < squares.size()+1)
			squares[num - 1].setType(_t);
	}

	void draw(HDC context)
	{
		HBRUSH fill, rectangle;
		HPEN pen, frame;
		COLORREF color = RGB(255, 255, 255);
		pen = CreatePen(PS_SOLID, 1, color);
		fill = CreateSolidBrush(color);
		rectangle = (HBRUSH)SelectObject(context, fill);
		frame = (HPEN)SelectObject(context, pen);
		Rectangle(context, position.X, position.Y, w * (square_size + spacing) + spacing + position.X, h * (square_size + spacing) + spacing + position.Y);
		SelectObject(context, rectangle);
		SelectObject(context, frame);
		DeleteObject(fill);
		DeleteObject(pen);

		for (unsigned i = 0; i < squares.size(); ++i)
			squares[i].draw(context);
	}
};