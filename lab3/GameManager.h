#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include "Grid.h"
#include "Player.h"
#define NUM_GAMESTATES 3
#define BET_STATE 0
#define ROLLING_STATE 1
#define RESULT_STATE 2
#define ANIM_PLAY 1101
#define ANIM_TICK 1102
#define ANIM_STOP 1100
#define NUM_TICKS 13
#define TICK_TIME 100
#define CHECK_FINISH 1003
#define CHECK_START 1002
#define RESET_START 1004
//-----------------------------bet > waiting for score > result
using namespace std;

void refresh(HWND hwnd)
{
	InvalidateRect(hwnd, NULL, true);
	UpdateWindow(hwnd);
}

int randBetween(int min, int max)
{
	return (rand() % (max - min + 1)) + min;
}

class GAMEMANAGER 
{
private:
	int keys[0xFF+1];
public:
	GRID grid;
	PLAYER player;
	HWND hwnd;
	int gamestate = 0;
	int counter = 0;
	int animstate = 0;
	int lastTry = 0;
	int lastShot = 0;
	bool lastResult = false;
	RECT statePlace;
	string statelog = "";
	UINT timerone = 0;
	UINT timertwo = 0;
	int tickno = 0;
	int lastTick = 0;
	bool blink = true;

	GAMEMANAGER(void)
	{
		player.name = "Player 1";
	}

	void counterInc(void)
	{
		counter++;
		gamestate = counter % NUM_GAMESTATES;
		buff = "counter: " + to_string(counter) + "\t\tgamestate: " + to_string(gamestate) +"  " + statelog + "\n";
		OutputDebugString(buff.c_str());
	}

	bool checkHit(int t_)
	{
		lastResult = false;
		lastTry = t_;
		if (t_)
		{
			grid.reset();
			if (t_ <= grid.getSize())
			{
				lastShot = randBetween(1, grid.getSize());
				lastResult = (lastShot == lastTry);
				if (gamestate == BET_STATE)
				{
					counterInc();
					timerone = SetTimer(hwnd, CHECK_START, 200, NULL);
					lastTick = lastTry;
					refresh(hwnd);
				}
			}
		}
		return lastResult;
	}

	bool click(int _x, int _y)
	{
		return checkHit(grid.check(_x, _y));
	}

	bool pressKey(int k)
	{
		if (keys[k])
			return false;
		else 
		{
			keys[k] = 1;
			//if (gamestate == BET_STATE)
			//	counterInc();
			return true;
		}
	}

	void liftKey(int k)
	{
		keys[k] = 0;
	}

	void setPlace(int _x1, int _y1, int _x2, int _y2)
	{
		statePlace.left = _x1;
		statePlace.top = _y1;
		statePlace.right = _x2;
		statePlace.bottom = _y2;
	}

	void draw(HDC context)
	{
		switch (gamestate)
		{
		case BET_STATE:
			statelog = "Bet... If you dare!";
			break;
		case ROLLING_STATE:
			statelog = "You bet #" + to_string(lastTry) + " - Checking...";
			break;
		case RESULT_STATE:
			if (lastResult)
			{
				statelog = "Congratz!";
			}
			else
			{
				statelog = "Sorry, you missed :(";
			}
			break;
		}
		//OutputDebugString((statelog + "\n").c_str());
		SetBkColor(context, RGB(0, 0, 0));
		SetBkMode(context, TRANSPARENT);
		SetTextColor(context, RGB(255, 255, 255));
		grid.draw(context);
		player.draw(context);
		HFONT hFont = CreateFont(24, 0, 0, 0, FW_MEDIUM, 0, 0, 0, 0, 0, 0, CLEARTYPE_QUALITY, 0, "Seoge UI");
		HFONT hTmp = (HFONT)SelectObject(context, hFont);
		DrawText(context, statelog.c_str(), -1, &statePlace, DT_TOP | DT_LEFT);
		DeleteObject(SelectObject(context, hTmp));
	}
};