#include "stdafx.h"
#include "lab3.h"
#include <iostream>
#include <string>
#include <vector>
#include "Results.h"
using namespace std;

class PLAYER
{
public:
	RESULT result;
	string name;
	RECT place;

	PLAYER(void)
	{
		name = "Player 1";
	}

	void setPlace(int _x1, int _y1, int _x2, int _y2)
	{
		place.left = _x1;
		place.top = _y1;
		place.right = _x2;
		place.bottom = _y2;
	}

	void draw(HDC context)
	{
		//TextOut(context, 10, 10, name.c_str(), name.length());
		HFONT hFont = CreateFont(24, 0, 0, 0, FW_MEDIUM, 0, 0, 0, 0, 0, 0, CLEARTYPE_QUALITY, 0, "Seoge UI");
		HFONT hTmp = (HFONT)SelectObject(context, hFont);
		buff = name + ":\n\n";
		buff += "Points:   " + to_string(result.getScore()) + "\n";
		buff += "Tries:     " + to_string(result.getTries()) + "\n";
		buff += "Ratio:     " + to_string((int)result.getRate()) + "%";
		DrawText(context, buff.c_str(), -1, &place, DT_TOP | DT_LEFT);
		DeleteObject(SelectObject(context, hTmp));
	}
};