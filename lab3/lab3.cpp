// lab3.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "lab3.h"
#include <iostream>
#include <string>
#include <vector>
#include "GameManager.h"
#include <time.h>

#define MAX_LOADSTRING 100
#define BTN_QUIT  1000
#define BTN_RESET 1001
#define CONF_RESET 1011
#define CONF_QUIT 1010
#define SCREENX GetSystemMetrics(SM_CXSCREEN)
#define SCREENY GetSystemMetrics(SM_CYSCREEN)
#define WINSIZEX 600
#define WINSIZEY 400
#define WIN_FILL_STYLE 

using namespace std;

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
HWND quitButton;
HWND resetButton;
PAINTSTRUCT ps;
HDC hdc;
HFONT buttonFont;


HBRUSH win_fill_style = (HBRUSH)(COLOR_WINDOW + 3); //+1 is white-default
GAMEMANAGER myGame;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_LAB3, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LAB3));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize			= sizeof(WNDCLASSEX);
	wcex.style			= CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LAB3));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= win_fill_style;
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }
   SetFocus(hWnd);

   quitButton = CreateWindowEx(0, "BUTTON", "QUIT", WS_CHILD | WS_VISIBLE, 100, 100, 150, 30, hWnd, HMENU(BTN_QUIT), hInstance, NULL);
   resetButton = CreateWindowEx(0, "BUTTON", "RESET", WS_CHILD | WS_VISIBLE, 100, 150, 150, 30, hWnd, HMENU(BTN_RESET), hInstance, NULL);
   myGame.grid = GRID(2, 2);
   myGame.hwnd = hWnd;
   srand(time(NULL));

   buttonFont = CreateFont(18, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, FALSE, ANSI_CHARSET,
	   OUT_TT_ONLY_PRECIS, CLIP_DEFAULT_PRECIS, CLEARTYPE_QUALITY,
	  DEFAULT_PITCH | FF_DONTCARE, TEXT("Segoe UI"));

   SendMessage(quitButton, WM_SETFONT, WPARAM(buttonFont), TRUE);
   SendMessage(resetButton, WM_SETFONT, WPARAM(buttonFont), TRUE);

   SetWindowPos(hWnd, NULL, (SCREENX-WINSIZEX)/2, (SCREENY-WINSIZEY)/2, WINSIZEX, WINSIZEY, NULL);
   UpdateWindow(hWnd);
   ShowWindow(hWnd, nCmdShow);
   myGame.grid.adjustToPossibleSpace(10, 10, WINSIZEX - 155, WINSIZEY - 36);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT window;
	int width = 0, height = 0;
	string buff = "";

	switch (message)
	{
	case WM_LBUTTONDOWN: //left-button-down
	{
		int mousex = LOWORD(lParam), mousey = HIWORD(lParam);
		if (myGame.gamestate == BET_STATE)
			myGame.click(mousex, mousey);
		break;
	}

	case WM_LBUTTONDBLCLK: //double-click 
	{
		int mousex = LOWORD(lParam), mousey = HIWORD(lParam);
		if (myGame.gamestate == BET_STATE)
			myGame.click(mousex, mousey);
		break;
	}

	case WM_KEYDOWN:
	{
		//buff = "KeyPressedId: " + to_string(wmId-48) + "\n";
		//OutputDebugString(buff.c_str());
		int wmId, wmEvent;
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		if (myGame.pressKey(wmId))
		{
			if (wmId >= 0x30 && wmId <= 0x39)
			{
				if (myGame.gamestate == BET_STATE)
					myGame.checkHit(wmId - 48);
			}
			else
				switch (wmId)
				{
				case VK_ESCAPE:
					DestroyWindow(hWnd);
					break;
				}
		}
		break;
	}

	case WM_KEYUP:
	{
		int wmId, wmEvent;
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		myGame.liftKey(wmId);
		break;
	}
	case WM_COMMAND:
	{
		int wmId, wmEvent;
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case BTN_QUIT:
			DestroyWindow(hWnd);
			break;
		case BTN_RESET:
			myGame.grid.reset();
			myGame.player.result.reset();
			myGame.gamestate = BET_STATE;
			myGame.counter = BET_STATE;
			myGame.animstate = ANIM_STOP;
			myGame.tickno = 0;
			KillTimer(hWnd, myGame.timerone);
			refresh(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
			break;
		}
		SetFocus(hWnd);
		break;
	}
	case WM_TIMER:
	{
		int wmId, wmEvent;
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ANIM_PLAY:
			if (myGame.tickno < NUM_TICKS)
			{
				KillTimer(hWnd, myGame.timerone);
				myGame.grid.reset();
				if (myGame.lastTick > myGame.grid.getSize())
					myGame.lastTick = 0;
				myGame.grid.tag(myGame.lastTick++, 3);
				refresh(hWnd);
				myGame.tickno++;
				myGame.timerone = SetTimer(hWnd, ANIM_PLAY, TICK_TIME*(float)(1 + (float)(myGame.tickno / 10)), NULL);
			}
			else
			{
				KillTimer(hWnd, myGame.timerone);
				myGame.tickno = 0;
				refresh(hWnd);
				myGame.timerone = SetTimer(hWnd, ANIM_STOP, 500, NULL);
			}
			break;
		case ANIM_STOP:
			if (myGame.tickno < 7)
			{
				KillTimer(hWnd, myGame.timerone);
				myGame.grid.reset();
				if (myGame.blink)
					myGame.grid.tag(myGame.lastShot, 3);
				myGame.blink = !myGame.blink;
				refresh(hWnd);
				myGame.tickno++;
				myGame.timerone = SetTimer(hWnd, ANIM_STOP, 100, NULL);
			}
			else
			{
				KillTimer(hWnd, myGame.timerone);
				myGame.tickno = 0;
				refresh(hWnd);
				myGame.timerone = SetTimer(hWnd, CHECK_FINISH, 10, NULL);
			}
			break;
		case CHECK_START:
			KillTimer(hWnd, myGame.timerone);
			refresh(hWnd);
			myGame.timerone = SetTimer(hWnd, ANIM_PLAY, TICK_TIME, NULL);
			break;
		case CHECK_FINISH:
			myGame.grid.reset();
			KillTimer(hWnd, myGame.timerone);
			myGame.counterInc();
			myGame.grid.tag(myGame.lastTry, 2);
			myGame.grid.tag(myGame.lastShot, 1);
			myGame.player.result.count(myGame.lastShot == myGame.lastTry);
			refresh(hWnd);
			myGame.timerone = SetTimer(hWnd, RESET_START, 700, NULL);
			break;
		case RESET_START:
			KillTimer(hWnd, myGame.timerone);
			myGame.grid.reset();
			myGame.counterInc();
			refresh(hWnd);
			break;
		}
		break;
	}
	case WM_PAINT:
		//OutputDebugString("dupa");
		GetWindowRect(hWnd, &window);
		width = window.right - window.left;
		height = window.bottom - window.top;
		SetWindowPos(quitButton, NULL, width - (115 + 20), 20, 100, 30, NULL);
		SetWindowPos(resetButton, NULL, width - (115 + 20), 70, 100, 30, NULL);
		myGame.grid.adjustToPossibleSpace(10, 10, width - 155, height - 66);
		myGame.setPlace(10, height-66, width, height);
		myGame.player.setPlace(width - 135, height - 200, width, height);
		hdc = BeginPaint(hWnd, &ps);

		//FillRect(hdc, &ps.rcPaint, win_fill_style); //refill window with paint
		myGame.draw(hdc);

		EndPaint(hWnd, &ps);
		break;

	case WM_CLOSE:
		DestroyWindow(hWnd);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	case WM_GETMINMAXINFO:
	{
		MINMAXINFO* mmi = (MINMAXINFO*)lParam;
		mmi->ptMinTrackSize.x = WINSIZEX;
		mmi->ptMinTrackSize.y = WINSIZEY;
		return 0;
	}

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	
	}
	return 0;
}