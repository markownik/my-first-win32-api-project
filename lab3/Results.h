#include "stdafx.h"
#include "lab3.h"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

class RESULT
{
private:
	int score = 0, tries = 0;
public:
	void count(int p)
	{
		tries++;
		if (p > 0)
			score++;
	}

	void count(bool p)
	{
		tries++;
		if (p)
			score++;
	}

	int getScore(void)
	{
		return score;
	}

	int getTries(void)
	{
		return tries;
	}

	float getRate(void)
	{
		if (tries != 0)
			return (float)((float)score / (float)tries)*100;
		else
			return (float)0.0;
	}

	void reset(void)
	{
		score = 0;
		tries = 0;
	}
};