#include "stdafx.h"
#include "lab3.h"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

class SQUARE {
private:
	int size = 0, type = 0;
	COORD position;
public:
	SQUARE(void)
	{
		size = 10;
		type = 0;
	}

	SQUARE(int _x, int _y, int s, int t)
	{
		position.X = _x;
		position.Y = _y;
		size = s;
		type = t;
	}

	void setSize(int s)
	{
		size = s;
	}

	void setType(int _t)
	{
		type = _t;
	}

	void setPosition(int x, int y)
	{
		position.X = x;
		position.Y = y;
	}

	RECT getSquare(void)
	{
		RECT temp;
		temp.left = position.X;
		temp.top = position.Y;
		temp.right = position.X + size;
		temp.bottom = position.Y + size;
		return temp;
	}

	void draw(HDC context)
	{
		HBRUSH fill, rectangle;
		HPEN pen, frame;
		COLORREF color = RGB(0, 0, 0);
		switch (type)
		{
		case 0:
			color = RGB(0, 0, 0);
			break;
		case 1:
			color = RGB(0, 255, 0);
			break;
		case 2:
			color = RGB(255, 0, 0);
			break;
		case 3:
			color = RGB(255, 255, 0);
			break;
		}
		pen = CreatePen(PS_SOLID, 1, color);
		fill = CreateSolidBrush(color);
		rectangle = (HBRUSH)SelectObject(context, fill);
		frame = (HPEN)SelectObject(context, pen);
		Rectangle(context, position.X, position.Y, position.X + size, position.Y + size);
		SelectObject(context, rectangle);
		SelectObject(context, frame);
		DeleteObject(fill);
		DeleteObject(pen);
	}
};